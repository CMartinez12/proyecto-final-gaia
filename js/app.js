//En vía de extincion
var nodes = [{ id: 1, value: .5, x: 0, y: 1, z: -3.3, label: "Lobo gris" },
             { id: 2, value: .5, x: -1, y: 1.5, z: -2.9, label: "Gato Montes"},
             { id: 3, value: .5, x: 0, y: 1.7, z: -3, label: "Huron"},
    {
        id: 4,
        value: .5,
        x: .5,
        y: 1.5,
        z: -3,
        label: "Bufalo"
    },
    {
        id: 5,
        value: .5,
        x: -1.75,
        y: 2.7,
        z: -1.2,
        label: "Foca arpa"
    },
    {
        id: 6,
        value: .5,
        x: -.4,
        y: 0.1,
        z: -3.4,
        label: "Murcielago orejudo"
    },
    {
        id: 7,
        value: .5,
        x: -.6,
        y: -0.1,
        z: -3.4,
        label: "Oso caballo"
    },
    {
        id: 8,
        value: .5,
        x: -1.4,
        y: -.6,
        z: -3.1,
        label: "Mono titi cabeciblanco"
    },
    {
        id: 9,
        value: .5,
        x: -1.4,
        y: -2,
        z: -2.4,
        label: "Hemuel"
    },
    {
        id: 10,
        value: .5,
        x: -1.8,
        y: -2,
        z: -2.1,
        label: "Tapir"
    },
    {
        id: 11,
        value: .5,
        x: -2,
        y: -1.2,
        z: -2.5,
        label: "Nutria Gigante"
    },
    {
        id: 12,
        value: .5,
        x: -1.4,
        y: -1.2,
        z: -2.9,
        label: "Gato de los Andes"
    },
    {
        id: 13,
        value: .5,
        x: -.3,
        y: .2,
        z: 3.43,
        label: "Tigre de Bengála"
    },
    {
        id: 14,
        value: .5,
        x: -.7,
        y: .7,
        z: 3.3,
        label: "Panda rojo"
    },
    {
        id: 15,
        value: .5,
        x: 0,
        y: 1.7,
        z: 3,
        label: "Caballo de Mongolia"
    },
    {
        id: 16,
        value: .5,
        x: -2.25,
        y: .7,
        z: 2.5,
        label: "Acinonyx jubatus"
    },
    {
        id: 17,
        value: .5,
        x: -1.4,
        y: 1.5,
        z: 2.76,
        label: "Leopardo de las nieves"
    },
    {
        id: 18,
        value: .5,
        x: -3.2,
        y: 1.2,
        z: -.4,
        label: "Lince ibérico"
    },
    {
        id: 19,
        value: .5,
        x: -1.458,
        y: 2.6,
        z: 1.7,
        label: "Bisón europe"
    },
    {
        id: 20,
        value: .5,
        x: -2.78,
        y: 2.01,
        z: -.1,
        label: "Macaco de berberia"
    },
    {
        id: 21,
        value: .5,
        x: -2.95,
        y: 1.78,
        z: .1,
        label: "Cervus elaphus"
    },
    {
        id: 22,
        value: .5,
        x: -2.59,
        y: 2,
        z: 1,
        label: "Bisón bonasus"
    },
    {
        id: 23,
        value: .5,
        x: -3,
        y: -1,
        z: 1.2,
        label: "Elefante africano"
    },
    {
        id: 24,
        value: .5,
        x: -2.9,
        y: -0.8,
        z: 1.7,
        label: "Rinoceronte blanco"
    },
    {
        id: 25,
        value: .5,
        x: -3,
        y: -1.4,
        z: 1,
        label: "Liacon de Angola"
    },
    {
        id: 26,
        value: .5,
        x: -2.7,
        y: -0.5,
        z: 2,
        label: "Burro salvaje"
    },
    {
        id: 27,
        value: 2,
        x: -3.4,
        y: -0.5,
        z: -0.4,
        label: "Hipopótamo de Guinea"
    },

{
  id: 28,
  value:2,
x: 1.15,
y: -2.8,
z:1.5,
label:" Demonio de tasmania"
},
{
id: 29,
  value:2,
x: 1.8,
y: -1.9,
z:2.2,
label:"notomy"

}
,
{
id:30,
value:2,
x:1.6,
y:-2.2,
z:2.2,
label:"wombat"
}
,
{
id:31,
value:2,
x:0.6,
y:-2.4,
z:2.4,
label:"numbat"

}
];
var edges = [{from: 0,to: 1,value: 2},
             {from: 2,to: 3,value: 2},
             {from: 1,to: 2,value: 2},
             {from: 0,to: 2,value: 2},
             {from: 0,to: 3,value: 2},
             {from: 0,to: 5,value: 2},
             {from: 5,to: 6,value: 2},
             {from: 6,to: 7,value: 2},
             {from: 7,to: 8,value: 2},
             {from: 1,to: 5,value: 2},
    {
        from: 1,
        to: 6,
        value: 2
    },
    {
        from: 1,
        to: 7,
        value: 2
    },
    {
        from: 7,
        to: 10,
        value: 2
    },
    {
        from: 7,
        to: 11,
        value: 2
    },
    {
        from: 11,
        to: 8,
        value: 2
    },
    {
        from: 10,
        to: 8,
        value: 2
    },
    {
        from: 10,
        to: 9,
        value: 2
    },
    {
        from: 8,
        to: 9,
        value: 2
    },
    {
        from: 1,
        to: 4,
        value: 2
    },
    {
        from: 17,
        to: 19,
        value: 2
    },
    {
        from: 17,
        to: 4,
        value: 2
    },
    {
        from: 17,
        to: 20,
        value: 2
    },
    {
        from: 19,
        to: 20,
        value: 2
    },
    {
        from: 17,
        to: 21,
        value: 2
    },
    {
        from: 20,
        to: 21,
        value: 2
    },
    {
        from: 19,
        to: 21,
        value: 2
    },
    {
        from: 19,
        to: 4,
        value: 2
    },
    {
        from: 17,
        to: 26,
        value: 2
    },
    {
        from: 26,
        to: 23,
        value: 2
    },
    {
        from: 26,
        to: 22,
        value: 2
    },
    {
        from: 23,
        to: 22,
        value: 2
    },
    {
        from: 22,
        to: 24,
        value: 2
    },
    {
        from: 23,
        to: 24,
        value: 2
    },
    {
        from: 23,
        to: 25,
        value: 2
    },
    {
        from: 25,
        to: 15,
        value: 2
    },
    {
        from: 15,
        to: 16,
        value: 2
    },
    {
        from: 15,
        to: 13,
        value: 2
    },
    {
        from: 16,
        to: 14,
        value: 2
    },
    {
        from: 13,
        to: 14,
        value: 2
    },
    {
        from: 13,
        to: 12,
        value: 2
    },
    {
        from: 14,
        to: 12,
        value: 2
    },
    {
        from: 14,
        to: 15,
        value: 2
    },
    {
        from: 14,
        to: 18,
        value: 2
    },
    {
        from: 16,
        to: 18,
        value: 2
    },
    {
        from: 18,
        to: 21,
        value: 2
    },
    {
        from: 15,
        to: 12,
        value: 2
    },
];
//Extintos
var nodes1 = [

{ id: 1, value: .5, x: -.2, y: 2.3, z: -2.55, label: "Puma del este" },
             { id: 2, value: .5, x: .1, y: .5, z: -3.4, label: "Oso gris mexicano"},

             { id: 3, value: .5, x: -1.6, y: 0.1, z: -3.05, label: "Foca monje caribe"},

    {
        id: 4,
        value: .5,
        x: -1,
        y:-2.8,
        z: -1.75,
        label: "guara"
    },
    {
        id: 5,
        value: .5,
        x: -2.4,
        y: -1.2,
        z: -2.15,
        label: "raton candango"
    },

    {
        id: 6,
        value: .5,
        x: -3.18,
        y: 1.3,
        z: -.2,
        label: "Bucaro"
    },
    {
        id: 7,
        value: .5,
        x: -2.68,
        y: 2,
        z: .8,
        label: "tarpa polonia rusa"
    },

    {
        id: 8,
        value: 2,
        x: -2.6,
        y: -2.1,
        z: 0.7,
        label: "hipotrago azul"
    },
    {
        id: 9,
        value: 2,
        x: -3.4,
        y: 0.6,
        z: -0.3,
        label: "Bubal harebeest"
    },
//asia
    {
        id: 10,
        value: .5,
        x: .5,
        y: -1.2,
        z: -3.2,
        label: "Tigre bali"
    },
    {
        id: 11,
        value: .5,
        x: .01,
        y: 0,
        z: 3.45,
        label: "Ciervo de Schomburgk:"
    },

    {
        id: 12,
        value: 2,
        x: 0.8,
        y: -2,
        z: 2.7,
        label: "Canguro occidental"
    },
    {
        id: 13,
        value: 2,
        x: 0.3,
        y: -1.2,
        z: 3.2,
        label: "tigre de java"
    }
];
var edges1 = [{from: 0,to: 1,value: 2},
             {from: 2,to: 3,value: 2},
             {from: 3,to: 4,value: 2},
             {from: 4,to: 5,value: 2},
             {from: 5,to: 6,value: 2},
             {from: 6,to: 7,value: 2},
             {from: 7,to: 8,value: 2},
             {from: 8,to: 9,value: 2},
             {from: 9,to: 10,value: 2},
             {from: 10,to: 11,value: 2},
             {from: 11,to: 12,value: 2}
];

var scene, aspect, camera, renderer, controls;
var ambient, hemisphereluz, luzdown, luzup;
var earthgeo, earthmat, earthSphere;


init();
animate();
render();

function init() {
    //INICIALIZACIÓN
    scene = new THREE.Scene();
    aspect = window.innerWidth / window.innerHeight;
    camera = new THREE.PerspectiveCamera(75, aspect, 0.1, 1000);
    scene.add(camera);
    renderer = new THREE.WebGLRenderer({
        antialias: true,
        alpha: true
    });
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);
    controls = new THREE.OrbitControls(camera, renderer.domElement);

    //ELEMENTOS COMUNES
    var size = 10;
    var arrowSize = 1;
    var divisions = size;
    var origin = new THREE.Vector3(0, 0, 0);
    var x = new THREE.Vector3(1, 0, 0);
    var y = new THREE.Vector3(0, 1, 0);
    var z = new THREE.Vector3(0, 0, 1);
    var color = new THREE.Color(0x333333);
    var colorR = new THREE.Color(0xAA3333);
    var colorG = new THREE.Color(0x33AA33);
    var colorB = new THREE.Color(0x333366);

    //CREAR ILUMINACIÓN
    ambientLight = new THREE.AmbientLight(0xE5D5D5);
    ambientLight.intensity = .2;
    hemisphereluz = new THREE.HemisphereLight(0x27E89A8, 0xB1B8CA, .7); //LUZ DEL MEDIO
    luzdown = new THREE.DirectionalLight(0x136D69, .5); //LUZ DE ARRIBA
    luzup = new THREE.DirectionalLight(0xF9EDD3, .6); //LUZ AZUL DE ABAJO
    luzdown.position.set(200, -350, 0);
    scene.add(ambient, hemisphereluz, luzdown, luzup);


    //FONDO
    // GLOBO TERRAQUEO BASE
    var texture = new THREE.ImageUtils.loadTexture("assets/textures/texture1.jpg");
    //var texture = new THREE.TextureLoader().load('assets/textures/texture3.jpg');
    // texture.wrapS = THREE.RepeatWrapping;
    // texture.wrapT = THREE.RepeatWrapping;
    // texture.repeat.set( 4, 4 );

    earthgeo = new THREE.OctahedronBufferGeometry(3.4, 3);
    earthmat = new THREE.MeshPhongMaterial({
        shininess: 10,
        color: 0xD7E1CE,
        map: texture,
        normalMap: texture,
        bumpScale: 0.0005,
        side: THREE.DoubleSide,
        shading: THREE.FlatShading
    });
    earthSphere = new THREE.Mesh(earthgeo, earthmat);
    scene.add(earthSphere);

    //Estrellas
    var starGeo = new THREE.Geometry();
    for (let i = 0; i < 6000; i++) {
        let star = new THREE.Vector3(
            Math.random() * 600 - 300,
            Math.random() * 600 - 300,
            Math.random() * 600 - 300
        );
        starGeo.vertices.push(star);
    }
    var starMaterial = new THREE.PointsMaterial({
        color: 0xaaaaaa,
        size: 0.2
    });
    var stars = new THREE.Points(starGeo, starMaterial);
    scene.add(stars);



    //En via de extincion
    let penviageo = new THREE.SphereGeometry(.06, 10, 10);
    let penviamat = new THREE.MeshBasicMaterial({
        color: 0x1b9bd3
    });
    let parent = locatePoints(penviageo, penviamat, nodes);

    //Extintos
    let pextintosgeo = new THREE.SphereGeometry(.06, 10, 10);
    let pextintosmat = new THREE.MeshBasicMaterial({
          color: 0xCA1919
    });
    let ptextintos = locatePoints(pextintosgeo, pextintosmat, nodes1);


    var linemat = new THREE.LineBasicMaterial({
        color: 0x1b9bd3
    });
    for (let i = 0; i < edges.length; i++) {
        let xInit = nodes[edges[i].from].x;
        let yInit = nodes[edges[i].from].y;
        let zInit = nodes[edges[i].from].z;

        let xFin = nodes[edges[i].to].x;
        let yFin = nodes[edges[i].to].y;
        let zFin = nodes[edges[i].to].z;
        var curve = new THREE.CubicBezierCurve3(
        	new THREE.Vector3(xInit, yInit, zInit),
              new THREE.Vector3((xInit + xFin),
                  (yInit + yFin),
                  (zInit + zFin)),
          new THREE.Vector3(xFin, yFin, zFin)
        );
        var points = curve.getPoints( 50 );
        // var points = [];
        // points.push(new THREE.Vector3(nodes[edges[i].from].x,
        //     nodes[edges[i].from].y,
        //     nodes[edges[i].from].z));
        // points.push(new THREE.Vector3(nodes[edges[i].to].x,
        //     nodes[edges[i].to].y,
        //     nodes[edges[i].to].z));
        var linegeometry = new THREE.BufferGeometry().setFromPoints(points);

        var line = new THREE.Line(linegeometry, linemat);
        parent.add(line);
    }

    var linemat1 = new THREE.LineBasicMaterial({
        color: 0xCA1919
    });
    for (let i1 = 0; i1 < edges1.length; i1++) {
        let xInit = nodes1[edges1[i1].from].x;
        let yInit = nodes1[edges1[i1].from].y;
        let zInit = nodes1[edges1[i1].from].z;
        let xFin = nodes1[edges1[i1].to].x;
        let yFin = nodes1[edges1[i1].to].y;
        let zFin = nodes1[edges1[i1].to].z;
        var curve1 = new THREE.CubicBezierCurve3(
        	new THREE.Vector3(xInit, yInit, zInit),
              new THREE.Vector3((xInit + xFin),
                  (yInit + yFin),
                  (zInit + zFin)),
          new THREE.Vector3(xFin, yFin, zFin)
        );
        var points1 = curve1.getPoints( 50 );
        // var points = [];
        // points.push(new THREE.Vector3(nodes[edges[i].from].x,
        //     nodes[edges[i].from].y,
        //     nodes[edges[i].from].z));
        // points.push(new THREE.Vector3(nodes[edges[i].to].x,
        //     nodes[edges[i].to].y,
        //     nodes[edges[i].to].z));
        var linegeometry1 = new THREE.BufferGeometry().setFromPoints(points1);

        var line1 = new THREE.Line(linegeometry1, linemat1);
        parent.add(line1);
    }

    scene.add(parent);
    scene.add(ptextintos);
    camera.position.x = 2;
    camera.position.z = 7;
    camera.lookAt(origin);

}

function animate() {
    requestAnimationFrame(animate);
    var time = -performance.now() * 0.00023;
    camera.position.x = 7 * Math.cos(time);
    camera.position.z = 7 * Math.sin(time);
    camera.lookAt(scene.position);
    render();
    controls.update();
}

function render() {
    renderer.render(scene, camera);
}

function locatePoints(geometry, material, points){
  let parentSpace = new THREE.Object3D();
  for (let i = 0; i < points.length; i++) {
      let point = new THREE.Mesh(geometry, material);
      point.translateX(points[i].x);
      point.translateY(points[i].y);
      point.translateZ(points[i].z);
      parentSpace.add(point);
  }
  return parentSpace;
}
